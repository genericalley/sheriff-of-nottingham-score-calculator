 	$(document).ready(function() {

 	  var players = 5;
    var detbread;
    var detp4;
    var detp5;

    $("#fewerplay").click(function(){
      players--;
      if (players==3){
        detbread = $("#breadrow").detach();
        detp4 = $(".p4-cell").detach();
        $(this).attr("disabled", "disabled");
      }
      else {
        detp5 = $(".p5-cell").detach();
        $("#moreplay").removeAttr("disabled");
      }
    })

    $("#moreplay").click(function(){
      players++;
      if (players==4){
        $("#fewerplay").removeAttr("disabled");
        $(".p3-cell").after(function(i){
          return detp4[i];
        });
        $("#cheeserow").after(detbread);
      }
      else {
        $(".p4-cell").after(function(i){
          return detp5[i];
        });
        $(this).attr("disabled", "disabled");
      }
    })

 	  $('#calcwin').click(function() {

 	    var winner = 0;
 	    var results = [];
      var royalgoods = [];
 	    var royalty = [];
 	    var scoring = [0,0,0,0,0];
 	    var types = [
 	      ["apple", 20, 15, 2],
 	      ["cheese", 15, 10, 3],
 	      ["bread", 15, 10, 3],
 	      ["chicken", 10, 5, 4],
 	      ["contraband", 0, 0, 1],
        ["gold", 0, 0, 1]
 	    ];
 	    var i, j;
      var mod_bread = 0;

      //build a matrix from the table with # of cards/points from contra & gold
 	    for (j = 0; j < players; j++) {
 	      i = 0;
 	      results[j] = [];
 	      $(".p" + (j + 1)).each(function() {
 	        results[j][i] = 1 * $(this).val();
 	        i++;
 	      });
 	    }

      //apply royal goods bonuses and pass to royalty function
      for (j=0;j<players;j++){
        i=0;
        royalgoods[j] = [];
        $(".p"+(j + 1)+"-rgb").each(function(){
          royalgoods[j][i] = results[j][i] + 1*$(this).val();
          i++;
        });
      }

 	    for (i = 0; i < results[0].length; i++) {
        //offset by 1 if only three players for use in the types matrix
        if (players==3 && i==2){mod_bread=1;}
        //determine which players are king/queen for each type of card
        royalty[i] = maxcalc(royalgoods, i, players);
  	    for (j = 0; j < players; j++) {
  	      if (royalty[i][0].indexOf(j) > -1) {
            //add king icon to player/type cell
	          $("#p" + (j + 1) + "-" + types[i+mod_bread][0]).append('<span class="glyphicon glyphicon-king" style="color:gold;"></span>');
  	        //add king score to total for player
  	        scoring[j] += Math.floor(types[i+mod_bread][1] / royalty[i][0].length);
  	        }
  	      if (royalty[i][1].indexOf(j) > -1) {
  	        $("#p" + (j + 1) + "-" + types[i+mod_bread][0]).append('<span class="glyphicon glyphicon-queen" style="color:silver;"></span>');
  	        scoring[j] += Math.floor(types[i+mod_bread][2] / royalty[i][1].length);
          }
	      }

        for (j = 0; j < players; j++) {
 	        scoring[j] += types[i+mod_bread][3] * results[j][i];
 	      }
      }

 	    var maxxy = [0, 0];
 	    for (i = 0; i < players; i++) {
 	      if (maxxy[0] < scoring[i]) {
 	        maxxy[0] = scoring[i];
 	        maxxy[1] = i;
 	      }
 	    }

 	    for (i = 0; i < players; i++) {
 	      $("#p" + (i + 1) + "-final").html(scoring[i]);
 	    }

 	    $("#winner")[0].innerHTML = $("#play" + (maxxy[1] + 1))[0].value + " wins! <div class='btn btn-primary' id='startover'>Restart?</div>";
 	  })
 	});


 	function maxcalc(arrayin, col, players) {
 	  //function passes a column, which is a type of resource.
 	  //0 is apples, 1 is cheese, &tc
 	  //returns an array: max val, # of max vals, array of kings, array of queens
 	  var i, count = 0;
 	  var max = -Infinity;
 	  var max2 = -Infinity;
 	  var kings = [];
 	  var queens = [];
 	  var type;

 	  max = arrayin[0][col];
 	  for (i = 0; i < players; i++) {
 	    if (max < arrayin[i][col]) {
 	      max2 = max;
 	      max = arrayin[i][col];
 	    } else if (max > arrayin[i][col] && max2 < arrayin[i][col]) {
 	      max2 = arrayin[i][col];
 	    }
 	  }
 	  for (i = 0; i < players; i++) {
 	    if (max == arrayin[i][col]) {
 	      kings.push(i);
 	    }
 	    if (max2 == arrayin[i][col]) {
 	      queens.push(i);
 	    }
 	  }
 	  if (kings.length > 1) {
 	    queens = [];
 	  }
 	  return [kings, queens];
 	}


  $('#startover').click(function() {
    location.reload();
  })
